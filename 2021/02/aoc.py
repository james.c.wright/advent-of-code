#!/bin/env python3
#


class Submarine:

  def __init__(self, *args, **kwargs):

    self.h_pos, self.v_pos, self.aim_pos = self._get_course_position()

  def solution_2b(self):
    return(self.h_pos*self.v_pos)

  def _get_course_position(self):

    h_pos = 0
    v_pos = 0
    aim_pos = 0
   
    with open('course.input','r') as f_input:

      for course_step in f_input.readlines():

        course_step = course_step.strip()

        direction, magnitude = course_step.split(' ')

        if direction == "forward":
          h_pos += int(magnitude)
          v_pos += int(magnitude)*aim_pos
          continue

        if direction == "down":
          aim_pos += int(magnitude)
          continue

        if direction == "up":
          aim_pos -= int(magnitude)
          continue

    print(h_pos, v_pos, aim_pos)
    return (h_pos, v_pos, aim_pos)


if __name__ == "__main__":

  sub = Submarine()

  print(sub.solution_2b())

