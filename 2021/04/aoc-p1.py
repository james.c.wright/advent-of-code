#!/bin/env python3
#
import logging
from pprint import pprint

logging.basicConfig(
    level=logging.DEBUG, format="[%(asctime)s %(levelname)s/%(funcName)s] %(message)s"
)
logger = logging.getLogger(__name__)


class Submarine:
    def __init__(self, *args, **kwargs):
        self.all_boards = self.get_boards_from_input_file()
        self.all_numbers = self.get_numbers_from_input_file()
        self.winning_board = self.get_winning_board()
        self.winning_board_score = self.get_winning_board_score()

        self.losing_board = self.get_board_win_order()

    def get_winning_board_score(self):
        logger.info("Calculating score for {}".format(self.winning_board))
        unmarked_numbers = []
        for row in self.winning_board:
            for val in row:
                if val not in self.winning_called_numbers:
                    unmarked_numbers.append(val)
        board_score = int(sum(unmarked_numbers)) * int(self.last_called_number)
        return board_score

    def check_for_winning_board(self, called_numbers):
        def _check_board(board):
            for row in board:
                if all(int(r) in called_numbers for r in row):
                    logger.info("Row matched {}".format(called_numbers))
                    pprint(board)
                    return board

            transposed_board = list(map(list, zip(*board)))
            for row in transposed_board:
                if all(int(r) in called_numbers for r in row):
                    logger.info("Column matched {}".format(called_numbers))
                    pprint(board)
                    return board

        winning_boards = []
        for board in self.all_boards:
            winning_board = _check_board(board)
            if winning_board:
                return winning_board

    def get_winning_board(self):
        called_numbers = []
        for called_number in self.all_numbers:
            logger.debug("Adding {} to called numbers.".format(called_number))
            called_numbers.append(int(called_number))
            self.last_called_number = called_number

            if len(called_numbers) < 5:
                continue

            winning_board = self.check_for_winning_board(called_numbers)
            if winning_board:
                logger.info(
                    "Last number called to match board was {}".format(
                        self.last_called_number
                    )
                )
                self.winning_called_numbers = called_numbers
                return winning_board

    def get_board_win_order(self):

        called_numbers = []
        for called_number in self.all_numbers:
            logger.debug("Adding {} to called numbers.".format(called_number))
            called_numbers.append(int(called_number))
            self.last_called_number = called_number

            if len(called_numbers) < 5:
                continue

    def get_numbers_from_input_file(self, fn="numbers.input"):
        with open(fn, "r") as f_input:
            return f_input.read().strip().split(",")

    def get_boards_from_input_file(self, fn="boards.input"):
        all_boards = []
        with open(fn, "r") as f_input:
            board = []
            for line in f_input.readlines():
                if line is "\n":
                    all_boards.append(board)
                    board = []
                    continue
                board.append([int(b) for b in line.strip().split()])

        return all_boards


if __name__ == "__main__":

    sub = Submarine()

    print(sub.winning_board_score)
    # assert sub.solution_3a() == 1997414, "Solution 3-A should be 1997414"
    # assert sub.solution_3b() == 1032597, "Solution 3-B should be 1032597"
