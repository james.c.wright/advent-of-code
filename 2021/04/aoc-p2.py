#!/bin/env python3
#
import logging
from pprint import pprint

logging.basicConfig(
    level=logging.DEBUG, format="[%(asctime)s %(levelname)s/%(funcName)s] %(message)s"
)
logger = logging.getLogger(__name__)


class Submarine:
    def __init__(self, *args, **kwargs):
        self.all_boards = self.get_boards_from_input_file()
        self.all_numbers = self.get_numbers_from_input_file()
        self.last_winning_board = self.get_board_win_order()[-1]
        self.last_winning_board_score = self.get_board_score(self.last_winning_board)

    def get_board_score(self, board):
        logger.info(
            "Calculating score for {} using last called number {}".format(
                board, self.last_called_number
            )
        )
        unmarked_numbers = []
        for row in board:
            for val in row:
                if val not in self.called_numbers:
                    unmarked_numbers.append(val)
        board_score = int(sum(unmarked_numbers)) * int(self.last_called_number)
        logger.info(">> {} <<".format(board_score))
        return board_score

    def check_if_board_wins(self, board, called_numbers):
        for row in board:
            if all(int(r) in called_numbers for r in row):
                logger.info("Row matched {}".format(called_numbers))
                pprint(board)
                return True

        transposed_board = list(map(list, zip(*board)))
        for row in transposed_board:
            if all(int(r) in called_numbers for r in row):
                logger.info("Column matched {}".format(called_numbers))
                pprint(board)
                return True

    def get_board_win_order(self):

        remaining_boards = [b for b in self.all_boards]
        winning_boards = []
        called_numbers = []
        for called_number in self.all_numbers:
            logger.debug("Adding {} to called numbers.".format(called_number))
            called_numbers.append(int(called_number))
            self.last_called_number = called_number

            if len(called_numbers) < 5:
                continue

            losing_boards = []
            for board in remaining_boards:
                if self.check_if_board_wins(board, called_numbers):
                    winning_boards.append(board)
                else:
                    losing_boards.append(board)
            remaining_boards = [b for b in losing_boards]
            self.called_numbers = called_numbers

            if not losing_boards:
                return winning_boards

    def get_numbers_from_input_file(self, fn="numbers.input"):
        with open(fn, "r") as f_input:
            return f_input.read().strip().split(",")

    def get_boards_from_input_file(self, fn="boards.input"):
        all_boards = []
        with open(fn, "r") as f_input:
            board = []
            for line in f_input.readlines():
                if line is "\n":
                    all_boards.append(board)
                    board = []
                    continue
                board.append([int(b) for b in line.strip().split()])

        return all_boards


if __name__ == "__main__":

    sub = Submarine()
