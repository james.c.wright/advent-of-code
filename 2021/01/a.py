from __future__ import print_function

with open('input.txt', 'r') as f_input:

  increased = 0
  last_depth = 0
  for depth in f_input.readlines():
    depth = int(depth)

    if last_depth == 0:
      pass
    elif depth > last_depth:    
      increased += 1

    last_depth = depth

  print('Depth increased {} times'.format(increased))
