from __future__ import print_function

with open('input.txt','r') as f_input:

  all_depths = [int(d) for d in f_input.readlines()]
  last_window_sum = 0
  increased = 0

  for idx in range(0,len(all_depths)):

    new_window = all_depths[idx:idx+3]

    if len(new_window) < 3:
      continue

    new_window_sum = sum(new_window)

    if last_window_sum == 0:
      pass
    elif new_window_sum > last_window_sum:
      increased += 1

    last_window_sum = new_window_sum
      

  print('Depth increased {} times'.format(increased))
