#!/bin/env python3
#
import logging

logging.basicConfig(
    level=logging.DEBUG, format="[%(asctime)s %(levelname)s/%(funcName)s] %(message)s"
)
logger = logging.getLogger(__name__)


class Submarine:
    def __init__(self, *args, **kwargs):
        self.set_diagnostic_report_values()
        self.set_ratings()

    def solution_3a(self):
        return self.gamma_rate_dec * self.epsilon_rate_dec

    def solution_3b(self):
        return self.life_support_rating

    def get_common_bits(self, bits):
        z_count = bits.count("0")
        o_count = bits.count("1")
        logger.debug("Found {} zeroes, {} ones".format(z_count, o_count))
        if z_count > o_count:
            return ("0", "1")
        else:
            return ("1", "0")

    def get_bits_transposed(self, bits):
        return list(map(list, zip(*bits)))

    def set_ratings(self):

        bit_list = self.diagnostic_bits
        bit_offset = 0

        def get_rating(bit_list, bit_offset, rating_type=None):
            logger.debug(
                "Bit list length is {}, offset is {}".format(len(bit_list), bit_offset)
            )
            if len(bit_list) == 1:
                logger.debug("return bit list {}".format("".join(bit_list[0])))
                return "".join(bit_list[0])

            bit_list_transposed = self.get_bits_transposed(bit_list)
            most_common_bit, least_common_bit = self.get_common_bits(
                bit_list_transposed[bit_offset]
            )

            bit_type = None
            if rating_type is "oxygen":
                bit_type = most_common_bit
            if rating_type is "co2":
                bit_type = least_common_bit

            logger.debug("{} is the {} common bit".format(bit_type, rating_type))

            new_bit_list = []
            for bits in bit_list:

                if bits[bit_offset] == bit_type:
                    logger.debug("Appending {}".format("".join(bits)))
                    new_bit_list.append(bits)

            bit_offset += 1
            return get_rating(new_bit_list, bit_offset, rating_type=rating_type)

        self.ox_rating_bin = "0b{}".format(
            get_rating(bit_list, bit_offset, rating_type="oxygen")
        )
        self.co2_rating_bin = "0b{}".format(
            get_rating(bit_list, bit_offset, rating_type="co2")
        )
        self.ox_rating_dec = int(self.ox_rating_bin, 2)
        self.co2_rating_dec = int(self.co2_rating_bin, 2)

        logger.debug(
            "Oxygen rating is {}, CO2 rating is {}".format(
                self.ox_rating_dec, self.co2_rating_dec
            )
        )

        self.life_support_rating = self.ox_rating_dec * self.co2_rating_dec

    def set_diagnostic_report_values(self):

        gamma_rate_bits = []
        epsilon_rate_bits = []
        with open("input", "r") as f_input:

            self.diagnostic_bits = report = [
                list(l.strip()) for l in f_input.readlines()
            ]
            self.diagnostic_bits_transposed = bits = self.get_bits_transposed(report)

            for bit in bits:

                gamma_bit, epsilon_bit = self.get_common_bits(bit)
                logger.debug("{} is most common".format(gamma_bit))
                gamma_rate_bits.append(gamma_bit)
                epsilon_rate_bits.append(epsilon_bit)

            self.gamma_rate_bin = "0b{}".format("".join(gamma_rate_bits))
            self.epsilon_rate_bin = "0b{}".format("".join(epsilon_rate_bits))
            self.gamma_rate_dec = int(self.gamma_rate_bin, 2)
            self.epsilon_rate_dec = int(self.epsilon_rate_bin, 2)


if __name__ == "__main__":

    sub = Submarine()

    assert sub.solution_3a() == 1997414, "Solution 3-A should be 1997414"
    assert sub.solution_3b() == 1032597, "Solution 3-B should be 1032597"

    logger.info("Solution 3-B: {}".format(sub.solution_3b()))
